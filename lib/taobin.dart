import 'dart:io';

class TaoBin {
  final Map<String, int> menu = {
    "Caramel Machiato": 60,
    "Espresso": 50,
    "Thai Tea": 50,
    "Yuzu Soda": 65,
    "Grape Soda": 40
  };

  TaoBin() {
    printWelcome();
    String menu = chooseMenu();
    int price = chooseSize(menu);
    printSummary(menu, price);
  }

  void printWelcome() {
    print("""

>>> Welcome to TaoBin <<<
We are drinking vending machine solution.
Please feel free to choose our menu

""");
  }

  void _printMenu() {
    print("""

-- Menu --
""");
    for (int i = 0; i < menu.keys.length; ++i) {
      print("${i + 1}) ${menu.keys.elementAt(i)}");
    }
  }

  String chooseMenu() {
    _printMenu();
    stdout.write("Choose menu: ");
    return menu.keys.elementAt(int.parse(stdin.readLineSync() ?? "0") + 1);
  }

  int chooseSize(String menu) {
    stdout.write("Choose size: \n1) S\n2) M\n3) L\n");
    int size = int.parse(stdin.readLineSync() ?? "0");
    switch (size) {
      case 1:
        return (this.menu[menu] ?? 0) + 0;
      case 2:
        return (this.menu[menu] ?? 0) + 10;
      case 3:
        return (this.menu[menu] ?? 0) + 15;
      default:
        return 0;
    }
  }

  void printSummary(String menu, int price) {
    print("Your order is $menu for $price ฿");
  }
}
