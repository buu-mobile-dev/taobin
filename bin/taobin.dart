import 'package:taobin/taobin.dart';
import 'dart:io';

void main(List<String> arguments) {
  do {
    TaoBin();
  } while (checkContinue());
}

bool checkContinue() {
  print("Finished Order");
  stdout.write("Do you want to continue next order['Y'/'n']: ");
  switch (stdin.readLineSync()) {
    case 'N':
    case 'n':
      return false;
    default:
      return true;
  }
}
